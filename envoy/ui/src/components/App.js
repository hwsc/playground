import React from 'react';
import Button from 'components/Button';
import SearchBar from 'components/SeachBar';
import Result from 'components/Result';
import { esClient } from 'api/elastic-search';
import { fooClient, barClient } from 'api/grpc-svc';
import { FooRequest } from 'protos/foo_pb';
import { BarRequest } from 'protos/bar_pb';

class App extends React.Component {
  state = { esHits: [], fooState: null, barState: null };

  handleES = async (term) => {
    const response = await esClient.post('_search', {
      query: {
        query_string: {
          query: term,
        },
      },
    });
    this.setState({ esHits: response.data.hits.hits });
  };

  handleFoo = async () => {
    const request = new FooRequest();
    request.setName('foo browser client');
    try {
      const response = await fooClient.serveFoo(request, {});
      console.log(response.getMessage());
      this.setState({ fooState: true });
    } catch (err) {
      console.log(err);
      this.setState({ fooState: false });
    }
  };

  handleBar = async () => {
    const request = new BarRequest();
    request.setName('bar browser client');
    try {
      const response = await barClient.serveBar(request, {});
      console.log(response.getMessage());
      this.setState({ barState: true });
    } catch (err) {
      console.log(err);
      this.setState({ barState: false });
    }
  };

  renderGRPCStatus = (status) => {
    if (status === null) {
      return;
    }
    if (status) {
      return <i className="big thumbs up icon green" />;
    }
    return <i className="big thumbs down icon red" />;
  };

  render() {
    return (
      <div className="ui container padded center aligned raised segment">
        <h1>Test Envoy Proxy</h1>
        <div className="ui action input">
          <Button label="To Foo Service" handleClick={this.handleFoo} />
          {this.renderGRPCStatus(this.state.fooState)}
          <Button label="To Bar Service" handleClick={this.handleBar} />
          {this.renderGRPCStatus(this.state.barState)}
          <SearchBar onSubmit={this.handleES} />
        </div>
        <div className="ui row">
          <Result result={this.state.esHits} />
        </div>
      </div>
    );
  }
}

export default App;
