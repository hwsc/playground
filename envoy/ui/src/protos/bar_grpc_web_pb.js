/* eslint-disable */
/**
 * @fileoverview gRPC-Web generated client stub for protos
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!



const grpc = {};
grpc.web = require('grpc-web');

const proto = {};
proto.protos = require('./bar_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.protos.BarClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.protos.BarPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.protos.BarRequest,
 *   !proto.protos.BarResponse>}
 */
const methodDescriptor_Bar_ServeBar = new grpc.web.MethodDescriptor(
  '/protos.Bar/ServeBar',
  grpc.web.MethodType.UNARY,
  proto.protos.BarRequest,
  proto.protos.BarResponse,
  /**
   * @param {!proto.protos.BarRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.protos.BarResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.protos.BarRequest,
 *   !proto.protos.BarResponse>}
 */
const methodInfo_Bar_ServeBar = new grpc.web.AbstractClientBase.MethodInfo(
  proto.protos.BarResponse,
  /**
   * @param {!proto.protos.BarRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.protos.BarResponse.deserializeBinary
);


/**
 * @param {!proto.protos.BarRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.protos.BarResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.protos.BarResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.protos.BarClient.prototype.serveBar =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/protos.Bar/ServeBar',
      request,
      metadata || {},
      methodDescriptor_Bar_ServeBar,
      callback);
};


/**
 * @param {!proto.protos.BarRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.protos.BarResponse>}
 *     A native promise that resolves to the response
 */
proto.protos.BarPromiseClient.prototype.serveBar =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/protos.Bar/ServeBar',
      request,
      metadata || {},
      methodDescriptor_Bar_ServeBar);
};


module.exports = proto.protos;
