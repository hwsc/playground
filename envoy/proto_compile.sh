#!/bin/bash
set -euo pipefail

compile_protoc() {
  protoc -I . \
--plugin=protoc-gen-go=${GOPATH}/bin/protoc-gen-go \
--go_out=plugins=grpc,paths=source_relative:. \
--js_out=import_style=commonjs:./ui/src \
--grpc-web_out=import_style=commonjs,mode=grpcwebtext:./ui/src \
"$@"
}

compile_protoc protos/foo.proto
compile_protoc protos/bar.proto

JS_FILES="ui/src/protos/*"
for f in ${JS_FILES}
do
  echo -e "/* eslint-disable */\n$(cat "${f}")" > "${f}"
done