import { FooPromiseClient } from 'protos/foo_grpc_web_pb';
import { BarPromiseClient } from 'protos/bar_grpc_web_pb';

// http 2.0 POC with Envoy Proxy
export const fooClient = new FooPromiseClient(window._env_.API_URL, null, null);
export const barClient = new BarPromiseClient(window._env_.API_URL, null, null);
