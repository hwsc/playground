import React from 'react';

const Button = (props) => {
  return (
    <div>
      <button
        className="ui secondary button"
        type="button"
        onClick={props.handleClick}
      >
        {props.label}
      </button>
    </div>
  );
};

export default Button;
