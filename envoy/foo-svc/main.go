package main

import (
	"gitlab.com/hwsc/playground/envoy/protos"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"log"
	"net"
)

type service struct{}

func (s service) ServeFoo(ctx context.Context, in *protos.FooRequest) (*protos.FooResponse, error) {
	log.Printf("Called with %s\n", in.GetName())
	return &protos.FooResponse{
		Message: "ServeFoo",
	}, nil
}

func main() {
	log.Println("foo-svc initiating...")
	lis, err := net.Listen("tcp", ":50061")
	if err != nil {
		log.Fatalf("Failed to initialize TCP listener %v\n", err)
	}
	s := grpc.NewServer()
	protos.RegisterFooServer(s, service{})
	log.Println("foo-svc running...")
	reflection.Register(s)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("Failed to serve %v", err)
	}
}
