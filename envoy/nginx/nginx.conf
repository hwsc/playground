# let nginx determine how many workers
worker_processes auto;

events {
  # maximum number of simultaneous connections for a worker process
  worker_connections 100;
  # enable multiple connections at a time
  multi_accept on;
}

http {
  # required for styling
  include /etc/nginx/mime.types;

  log_format compression '$remote_addr - $remote_user [$time_local] '
  '"$request" $status $upstream_addr '
  '"$http_referer" "$http_user_agent"';

  server {
      # listen on specified port
      listen 80;

      access_log /var/log/nginx/access.log compression;

      # where the root here
      root /var/www;
      # the file to server as index
      index index.html;

      # it is highly possible we have to re-configure for single-page application routing
      location / {
        # serve request as file, then as directory, then fall back to redirecting to index.html
        try_files $uri $uri/ /index.html;
      }

      # route media files
      location ~* \.(?:jpg|jpeg|gif|png|ico|cur|gz|svg|svgz|mp4|ogg|ogv|webm|htc)$ {
        expires 1M;
        add_header Cache-Control "public";
      }

      # route direct access to file to 404
      location ~ ^.+\..+$ {
        try_files $uri =404;
      }
  }
}
