/* eslint-disable */
/**
 * @fileoverview gRPC-Web generated client stub for protos
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!



const grpc = {};
grpc.web = require('grpc-web');

const proto = {};
proto.protos = require('./foo_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.protos.FooClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.protos.FooPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.protos.FooRequest,
 *   !proto.protos.FooResponse>}
 */
const methodDescriptor_Foo_ServeFoo = new grpc.web.MethodDescriptor(
  '/protos.Foo/ServeFoo',
  grpc.web.MethodType.UNARY,
  proto.protos.FooRequest,
  proto.protos.FooResponse,
  /**
   * @param {!proto.protos.FooRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.protos.FooResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.protos.FooRequest,
 *   !proto.protos.FooResponse>}
 */
const methodInfo_Foo_ServeFoo = new grpc.web.AbstractClientBase.MethodInfo(
  proto.protos.FooResponse,
  /**
   * @param {!proto.protos.FooRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.protos.FooResponse.deserializeBinary
);


/**
 * @param {!proto.protos.FooRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.protos.FooResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.protos.FooResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.protos.FooClient.prototype.serveFoo =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/protos.Foo/ServeFoo',
      request,
      metadata || {},
      methodDescriptor_Foo_ServeFoo,
      callback);
};


/**
 * @param {!proto.protos.FooRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.protos.FooResponse>}
 *     A native promise that resolves to the response
 */
proto.protos.FooPromiseClient.prototype.serveFoo =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/protos.Foo/ServeFoo',
      request,
      metadata || {},
      methodDescriptor_Foo_ServeFoo);
};


module.exports = proto.protos;
