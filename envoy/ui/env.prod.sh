#!/bin/bash

if [[ ! -f .env ]]; then
  >&2 echo "missing .env file"
  exit 1
fi

OUT_DIR=${1:-.}
TARGET_FILE="${OUT_DIR}/env-config.js"

# Recreate config file
rm -rf ${TARGET_FILE}
touch ${TARGET_FILE}

# Add assignment
echo "window._env_ = {" >> ${TARGET_FILE}

# Read each line in .env file
# Each line represents key=value pairs
while read -r line || [[ -n "$line" ]];
do
  # Split env variables by character `=`
  if printf '%s\n' "$line" | grep -q -e '='; then
    var_name=$(printf '%s\n' "$line" | sed -e 's/=.*//')
    var_value=$(printf '%s\n' "$line" | sed -e 's/^[^=]*=//')
  fi

  # Read value of current variable if exists as Environment variable
  value=$(printf '%s\n' "${!var_name}")
  # Otherwise use value from .env file
  [[ -z $value ]] && value=${var_value}

  # Append configuration property to JS file
  echo "  $var_name: '$value'," >> ${TARGET_FILE}
done < .env

echo "};" >> ${TARGET_FILE}
