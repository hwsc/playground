import Axios from 'axios';

// http 1.1 POC with Envoy Proxy
export const esClient = Axios.create({
  baseURL: `${window._env_.API_URL}/elastic-search/`,
  timeout: 1000,
  headers: {
    'Content-Type': 'application/json',
  },
});
