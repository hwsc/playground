# Envoy Proxy POC

## Table of Contents
- [Envoy Proxy POC](#envoy-proxy-poc)
  - [Table of Contents](#table-of-contents)
    - [Purpose](#purpose)
    - [Overview](#overview)
      - [Application](#application)
      - [CI Job](#ci-job)
      - [CI Job Workflow](#ci-job-workflow)
      - [es-init](#es-init)
      - [grpc-web client](#grpc-web-client)
      - [References](#references)
    - [To get started](#to-get-started)

### Purpose
- To use [Enoyy](https://www.envoyproxy.io/) as a proxy for API gateway to different services.
- To ensure tests can internally and externally ran against an entire application running using `docker-compose` in GitLab CI.

### Overview
#### Application
```mermaid
graph TD;
    A[grpc-web client] --> C(Envoy proxy);
    B[CLI client] --> C(Envoy proxy);
    C(Envoy proxy) --> D(ElasticSearch);
    C(Envoy proxy) --> E(Foo Service);
    C(Envoy proxy) --> F(Bar Service);
    G(ES Init) -->|Put dummy data| D(ElasticSearch);
```

#### CI Job
```mermaid
graph BT
  subgraph Docker Compose Network
    A[Cypress]
    A-->|run UI test| B
    subgraph Z[Backend]
      style Z fill:#abe,stroke:#333,stroke-width:4px
      B[Envoy Proxy]
      C[Bar Service]
      B -->|proxy requests| C
      B -->|proxy requests| D
      B -->|proxy requests| E
      subgraph Y[Foo Service-ElasticSearch Dependency]
        style Y fill:#fa7,stroke:#333,stroke-width:4px
        D[Foo Service]
        E[(ElasticSearch)]
        D -->|run integration test|E
      end
      F[ES-init]
      F-->|initialize with dummy data| E
    end
  end
```

#### CI Job Workflow
```mermaid
sequenceDiagram
    participant Z as gitlab ci job
    participant A as cypress
    participant B as docker-compose
    participant C as dockerhub
    participant D as gitlab repo
      activate Z
      Z->>B: start application using docker-compose
      activate B
      B->>C: pull required images
      activate C
      C-->>B: send images
      deactivate C
      B->>D: build app images
      activate D
      D-->>B: copy required codes
      B-->>B: start services
      B-->>A: start cypress
      deactivate D
      activate A
      loop every N time
        rect rgba(107, 198, 250, 1.0)
          B-->>B: run application in the background
          A-->>B: is application ready?
        end
    end
    B->>A: application is ready
    A->>B: run api call or tests
    Note over A,B: If failure/timeout exists, then fail-fast
    A->>Z: notify pass or failure using exit status
    deactivate A
    deactivate B
    Z-->>Z: cleanup application
    Z-->>Z: generate artifacts
    Z-->>Z: pass or fail the job
    deactivate Z
```

#### es-init
Initializes ElasticSearch with some dummy data from the `envoy/es-init/books` directory.

#### grpc-web client
Ideally, we want to use the following UI components to test our Envoy proxy
- a search bar for ElasticSearch
- a button to make request to FooService
- a button to make request to BarService

![alt text](envoy-browser-poc.gif)

#### References
- [React](https://reactjs.org/)
- [Envoy Proxy](https://www.envoyproxy.io/docs)
- [Docker Hub docker-compose](https://hub.docker.com/r/docker/compose)
- [GRPC Official Doc](https://grpc.io/docs/tutorials/basic/web/)
- [GRPC Web React Example](https://github.com/longfellowone/grpcwebtest)
- [Sample Envoy Config File](https://medium.com/swlh/ditching-rest-with-grpc-web-and-envoy-bfaa89a39b32)
- [Envoy and gRPC-Web](https://blog.envoyproxy.io/envoy-and-grpc-web-a-fresh-new-alternative-to-rest-6504ce7eb880)
- [ElasticSearch w/ Dummy Data](https://blog.patricktriest.com/text-search-docker-elasticsearch/)
- [Quick Styling w/ Semantic UI](https://semantic-ui.com/introduction/getting-started.html)
- [Testing Website with Cypress and GitLab CI](https://cristian.livadaru.net/testing-websites-with-cypress-and-gitlab-ci/)
- [Running Cypress with GitLab CI](https://jessie.codes/article/running-cypress-gitlab-ci/)
- [Whitebox Golang Integration Test w/ Build Tags](https://harrigan.xyz/blog/integration-testing-with-golang/)

### To get started
- #### [UI README.md](https://gitlab.com/hwsc/playground/-/tree/master/envoy%2Fui)

- #### compile proto files to go and js
```bash
$ cd envoy
$ ./proto_compile.sh 
```

- #### build docker images
```bash
$ docker-compose build
```

- #### run docker composition
```bash
$ docker-compose up
```

- #### check services
    - to check GRPC services
    ```bash
    $ cd envoy/ui
    $ yarn start
    # if necessary, $ yarn install
    # open the browser and verify both services are working by pressing the buttons
    ```

    - to check ES
    ```bash
    # checks if es-init put dummy data
    $ curl -XPOST 'localhost:8080/elastic-search/_search?pretty' -H 'Content-Type: application/json' -d '{
        "query": { "query_string": { "query": "java" } }
      }'
    # results...
    {
      "took" : 23,
      "timed_out" : false,
      "_shards" : {
        "total" : 1,
        "successful" : 1,
        "skipped" : 0,
        "failed" : 0
      },
      "hits" : {
        "total" : {
          "value" : 12,
          "relation" : "eq"
        },
        "max_score" : 15.338156,
        "hits" : [
          {
            "_index" : "library",
            "_type" : "novel",
            "_id" : "XzDY73ABI7MzV8-rBBHJ",
            "_score" : 15.338156,
            "_source" : {
              "author" : "Charles Darwin",
              "title" : "On the Origin of Species",
              "location" : 1080,
              "text" : "Java, plants of, 375."
            }
          },
          {
            "_index" : "library",
            "_type" : "novel",
            "_id" : "ezHY73ABI7MzV8-rMEBQ",
            "_score" : 9.737752,
            "_source" : {
              "author" : "Edgar Allan Poe",
              "title" : "The Works of Edgar Allan Poe",
              "location" : 829,
              "text" : "We got under way with a mere breath of wind, and for many days stood along the eastern coast of Java, without any other incident to beguile the monotony of our course than the occasional meeting with some of the small grabs of the Archipelago to which we were bound."
            }
          },
    ... # and more
    ```
