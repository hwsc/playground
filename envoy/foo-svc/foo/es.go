package foo

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/elastic/go-elasticsearch/v7"
	"log"
	"os"
)

var (
	// ESClient is the client for making ES queries.
	ESClient *elasticsearch.Client
)

// ESResult is the summary of the search
type ESResult struct {
	Status string
	Hits   float64
	Took   float64
}

func init() {
	ESHost := func() string {
		addr := os.Getenv("HWSC_PLAYGROUND_ENVOY_ES")
		if addr == "" {
			return "localhost"
		}
		return addr
	}()
	cfg := elasticsearch.Config{
		Addresses: []string{
			fmt.Sprintf("http://%s:9200", ESHost),
		},
	}
	ESClient, _ = elasticsearch.NewClient(cfg)
}

// ESSearch search for a given term and return the summary of result or error
func ESSearch(term string) (*ESResult, error) {
	var r map[string]interface{}
	var buf bytes.Buffer
	var err error

	// make query string
	query := map[string]interface{}{
		"query": map[string]interface{}{
			"query_string": map[string]interface{}{
				"query": term,
			},
		},
	}
	if err = json.NewEncoder(&buf).Encode(query); err != nil {
		return nil, err
	}

	// perform the search request.
	res, err := ESClient.Search(
		ESClient.Search.WithContext(context.Background()),
		ESClient.Search.WithBody(&buf),
		ESClient.Search.WithTrackTotalHits(true),
		ESClient.Search.WithPretty(),
	)
	if err != nil {
		return nil, err
	}
	defer func() {
		_ = res.Body.Close()
	}()

	if res.IsError() {
		var e map[string]interface{}
		if err := json.NewDecoder(res.Body).Decode(&e); err != nil {
			return nil, err
		}
		// print the response status and error information.
		log.Fatalf("[%s] %s: %s",
			res.Status(),
			e["error"].(map[string]interface{})["type"],
			e["error"].(map[string]interface{})["reason"],
		)
		return nil, errors.New("response error")
	}

	if err := json.NewDecoder(res.Body).Decode(&r); err != nil {
		return nil, err
	}
	// print the response status, number of results, and request duration.
	hits := r["hits"].(map[string]interface{})["total"].(map[string]interface{})["value"].(float64)
	// simulate an error just for the fun of it
	if hits == 0 {
		return nil, errors.New("no hits")
	}
	took := r["took"].(float64)
	log.Printf(
		"[%s] %v hits; took: %vms",
		res.Status(),
		hits,
		took,
	)
	return &ESResult{Status: res.Status(), Hits: hits, Took: took}, nil
}
