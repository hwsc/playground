import React from 'react';

/*
Array(10)
  0:
    _index: "library"
    _type: "novel"
    _id: "-Rfu73ABCXS4_YQ3GGtg"
    _score: 15.285856
    _source:
      author: "Charles Darwin"
      title: "On the Origin of Species"
      location: 1080
      text: "Java, plants of, 375."
      __proto__: Object
      __proto__: Object
 */
const renderResult = (result) => {
  return result.map(({ _id, _source }) => {
    return (
      <div key={_id} className="column eight wide">
        <hr />
        <h4>
          {_source.author} - {_source.title} - {_source.location}
        </h4>
        <p>{_source.text}</p>
      </div>
    );
  });
};

const Result = (props) => {
  return renderResult(props.result);
};

export default Result;
