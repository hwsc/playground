package main

import (
	"gitlab.com/hwsc/playground/envoy/protos"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"log"
	"net"
)

type service struct{}

func (s service) ServeBar(ctx context.Context, in *protos.BarRequest) (*protos.BarResponse, error) {
	log.Printf("Called with %s\n", in.GetName())
	return &protos.BarResponse{
		Message: "ServeBar",
	}, nil
}

func main() {
	log.Println("bar-svc initiating...")
	lis, err := net.Listen("tcp", ":50062")
	if err != nil {
		log.Fatalf("Failed to initialize TCP listener %v\n", err)
	}
	s := grpc.NewServer()
	protos.RegisterBarServer(s, service{})
	log.Println("bar-svc running...")
	reflection.Register(s)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("Failed to serve %v", err)
	}
}
