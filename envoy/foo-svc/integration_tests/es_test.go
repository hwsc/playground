// +build integration

package integration_tests

import (
	"gitlab.com/hwsc/playground/envoy/foo-svc/foo"
	"testing"
)

func TestESSearch(t *testing.T) {
	tests := []struct {
		name    string
		term    string
		want    *foo.ESResult
		wantErr bool
	}{
		{
			"should get java hits",
			"java",
			&foo.ESResult{
				Status: "200 OK",
				Hits:   13,
				Took:   0,
			},
			false,
		},
		{
			"should get go hits",
			"go",
			&foo.ESResult{
				Status: "200 OK",
				Hits:   11849,
				Took:   0,
			},
			false,
		},
		{
			"should get zero hits",
			"gadasdo",
			nil,
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := foo.ESSearch(tt.term)
			if (err != nil) != tt.wantErr {
				t.Errorf("ESSearch() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !tt.wantErr {
				if got == nil {
					t.Error("got nil result")
					return
				}
				if got.Status != tt.want.Status {
					t.Errorf("ESSearch() Status = %v, want.Status %v", got.Status, tt.want.Status)
					return
				}
				if got.Hits != tt.want.Hits {
					t.Errorf("ESSearch() Hits = %v, want.Status %v", got.Hits, tt.want.Hits)
					return
				}
				if got.Took <= 0 {
					t.Errorf("ESSearch() Took = %vms", got.Took)
					return
				}
			}
		})
	}
}
