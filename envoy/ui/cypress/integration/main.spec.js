/// <reference types="cypress" />

describe.only('test main page', () => {
  beforeEach(() => {
    cy.visit('/');
    cy.get('[class="big thumbs up icon green"]').should('have.length', 0);
    cy.get('div[class="ui row"]').children().should('have.length', 0);
  });

  it('should access foo service using button', () => {
    cy.contains('To Foo Service')
      .click()
      .should('have.focus')
      .parent()
      .next()
      .should('have.class', 'big thumbs up icon green');
    cy.get('[class="big thumbs up icon green"]').should('have.length', 1);
  });

  it('should access bar service using button', () => {
    cy.contains('To Bar Service')
      .click()
      .should('have.focus')
      .parent()
      .next()
      .should('have.class', 'big thumbs up icon green');
    cy.get('[class="big thumbs up icon green"]').should('have.length', 1);
  });

  it('should access foo and bar services using buttons', () => {
    cy.contains('To Foo Service')
      .click()
      .should('have.focus')
      .parent()
      .next()
      .should('have.class', 'big thumbs up icon green');
    cy.get('[class="big thumbs up icon green"]').should('have.length', 1);
    cy.contains('To Bar Service')
      .click()
      .should('have.focus')
      .parent()
      .next()
      .should('have.class', 'big thumbs up icon green');
    cy.get('[class="big thumbs up icon green"]').should('have.length', 2);
  });

  it('should be able to search using input search bar', () => {
    Cypress.currentTest.retries(4);
    cy.get('form')
      .click()
      .find('input[placeholder="Search..."]')
      .should('have.focus')
      .type('java')
      .should('have.value', 'java');
    cy.get('form').submit();
    cy.get('div[class="ui row"]').children().should('not.have.length', 0);
  });

  it('should be able to use everything', () => {
    Cypress.currentTest.retries(4);
    cy.contains('To Foo Service')
      .click()
      .should('have.focus')
      .parent()
      .next()
      .should('have.class', 'big thumbs up icon green');
    cy.get('[class="big thumbs up icon green"]').should('have.length', 1);
    cy.contains('To Bar Service')
      .click()
      .should('have.focus')
      .parent()
      .next()
      .should('have.class', 'big thumbs up icon green');
    cy.get('[class="big thumbs up icon green"]').should('have.length', 2);
    cy.get('div[class="ui row"]').children().should('have.length', 0);
    cy.get('form')
      .click()
      .find('input[placeholder="Search..."]')
      .should('have.focus')
      .type('java')
      .should('have.value', 'java');
    cy.get('form').submit();
    cy.get('div[class="ui row"]').children().should('not.have.length', 0);
  });
});
