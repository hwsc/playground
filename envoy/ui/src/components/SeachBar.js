import React from 'react';

class SearchBar extends React.Component {
  state = { term: '' };

  handleChange = (event) => {
    event.preventDefault();
    this.setState({ term: event.target.value });
  };

  handleSubmit = (event) => {
    event.preventDefault();
    // pass the term back to App.js using callback function from props onSubmit
    this.props.onSubmit(this.state.term);
  };

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit} className="ui form">
          <div className="ui small icon input">
            <input
              type="text"
              placeholder="Search..."
              value={this.state.term}
              onChange={this.handleChange}
            />
            <i className="search icon" />
          </div>
        </form>
      </div>
    );
  }
}

export default SearchBar;
