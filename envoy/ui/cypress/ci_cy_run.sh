#!/bin/bash
set -euo pipefail
yarn prereport
cypress run --headless --browser ${CI_BROWSER:-chrome} --env configFile=production && yarn mochawesome:run && yarn junit:merge
